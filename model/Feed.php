<?php 
class Feed{
  
    private $title;
    private $body;
    private $image;
    private $source;
    private $publisher;
    
    function __construct($title,$body,$image,$source,$publisher){
        $this->title=$title;
        $this->body=$body;
        $this->image=$image;
        $this->source=$source;
        $this->publisher=$publisher;        
    }

    public function getTitle()
    {
        return $this->title;
    }


    public function setTitle($title)
    {
        $this->title = $title;
    }


    public function getBody()
    {
        return $this->body;
    }

   
    public function setBody($body)
    {
        $this->body = $body;
    }


    public function getImage()
    {
        return $this->image;
    }

 
    public function setImage($image)
    {
        $this->image = $image;
    }

 
    public function getSource()
    {
        return $this->source;
    }


    public function setSource($source)
    {
        $this->source = $source;
    }

  
    public function getPublisher()
    {
        return $this->publisher;
    }


    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;
    }

}
?>